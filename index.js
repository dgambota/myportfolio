const projrow1 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    
    if (entry.isIntersecting) {
      entry.target.querySelector('.proj-card').classList.add('fadeInLeftAnimation');
      entry.target.querySelector('.tag1').classList.add('fadeInTopAnimation1');
      entry.target.querySelector('.tag2').classList.add('fadeInTopAnimation2');
      entry.target.querySelector('.tag3').classList.add('fadeInTopAnimation3');
      entry.target.querySelector('.tag4').classList.add('fadeInTopAnimation4');
      entry.target.querySelector('.check-proj-btn').classList.add('fadeinViewProjBtn');
      entry.target.querySelector('.proj-description').classList.add('proj-descriptionAnimation');
	  return; // if we added the class, exit the function
    }
  });
});

const projrow2 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    
    if (entry.isIntersecting) {
      entry.target.querySelector('.proj-card').classList.add('fadeInLeftAnimation');
      entry.target.querySelector('.tag1').classList.add('fadeInTopAnimation1');
      entry.target.querySelector('.tag2').classList.add('fadeInTopAnimation2');
      entry.target.querySelector('.tag3').classList.add('fadeInTopAnimation3');
      entry.target.querySelector('.tag4').classList.add('fadeInTopAnimation4');
      entry.target.querySelector('.check-proj-btn').classList.add('fadeinViewProjBtn');
      entry.target.querySelector('.proj-description').classList.add('proj-descriptionAnimation');
    return; // if we added the class, exit the function
    }
  });
});

const projrow3 = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    
    if (entry.isIntersecting) {
      entry.target.querySelector('.proj-card').classList.add('fadeInLeftAnimation');
      entry.target.querySelector('.tag1').classList.add('fadeInTopAnimation1');
      entry.target.querySelector('.tag2').classList.add('fadeInTopAnimation2');
      entry.target.querySelector('.tag3').classList.add('fadeInTopAnimation3');
      entry.target.querySelector('.tag4').classList.add('fadeInTopAnimation4');
      entry.target.querySelector('.tag5').classList.add('fadeInTopAnimation5');    
      entry.target.querySelector('.check-proj-btn').classList.add('fadeinViewProjBtn');
      entry.target.querySelector('.proj-description').classList.add('proj-descriptionAnimation');
    return; // if we added the class, exit the function
    }
  });
});

const experience = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    
    if (entry.isIntersecting) {
      entry.target.querySelector('.row1').classList.add('fadeInTopAnimationMid1');
      entry.target.querySelector('.row2').classList.add('fadeInTopAnimationMid2');
      entry.target.querySelector('.row3').classList.add('fadeInTopAnimationMid3');
    return; // if we added the class, exit the function
    }
  });
});

const about = new IntersectionObserver(entries => {
  entries.forEach(entry => {
    
    if (entry.isIntersecting) {
      entry.target.querySelector('.row1').classList.add('fadeInTopAnimation1');
      entry.target.querySelector('.row2').classList.add('fadeInTopAnimation2');
      entry.target.querySelector('.row3').classList.add('fadeInTopAnimation3');
      entry.target.querySelector('.row4').classList.add('fadeInTopAnimation4');
    return; // if we added the class, exit the function
    }
  });
});

projrow1.observe(document.querySelector('#projrow1'));
projrow2.observe(document.querySelector('#projrow2'));
projrow3.observe(document.querySelector('#projrow3'));
experience.observe(document.querySelector('#experow'));
about.observe(document.querySelector('#aboutrow'));